#ifndef __SIMPLEX_OPENGL_GRAPHICS_API_ADAPTEE_H__
#define __SIMPLEX_OPENGL_GRAPHICS_API_ADAPTEE_H__

#include <Simplex/Graphics/GraphicsApiAdaptee.h>
#include <Simplex/OpenGL/third-party.h>
#include <vector>

namespace Simplex
{
  namespace OpenGL
  {
  	struct RenderableMapping 
  	{
  		Simplex::Graphics::Renderable* Renderable;
  		Simplex::Graphics::Mesh* Mesh;
      std::vector<Simplex::Graphics::Shader::Info> Shaders;
  		GLuint VertexArrayObject;
			GLuint Buffer;
      GLuint ShaderProgram;
			int VertexCount;
  	};

    class GraphicsApiAdaptee : public Simplex::Graphics::GraphicsApiAdaptee
    {
    
    public:
    	
      GraphicsApiAdaptee() {}
      void Initialize();
      void Shutdown();

      void AddRenderable ( Simplex::Graphics::Renderable& renderable );

      Simplex::Graphics::Mesh& GetLoadedMesh ( int index );
      void LoadMesh ( int index, Simplex::Graphics::Mesh* mesh );
      
      std::vector<Simplex::Graphics::Shader::Info>& GetLoadedShaders ( int index );
      void LoadShaders (int index, std::vector<Simplex::Graphics::Shader::Info>& shaders );
      void UseShaders ( int index );
      
      void Render ( int index );

    private:

    	std::vector<RenderableMapping> mRenderables;

    	void PrintErrors();

      GLuint CreateShader ( Simplex::Graphics::Shader::Info& shaderInfo );
      const char* ReadShader( const char* filename );
    	GLenum ShaderTypeMapping ( Simplex::Graphics::Shader::Type type );
    };
  }
}

#endif