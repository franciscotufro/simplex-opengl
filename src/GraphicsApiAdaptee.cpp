#include <Simplex/OpenGL/GraphicsApiAdaptee.h>
#include <Simplex/Core/Logger.h>
#include <Simplex/Graphics/Renderable.h>
#include <stdio.h>

using namespace Simplex::Core;

namespace Simplex
{

  namespace OpenGL
  {
    void GraphicsApiAdaptee::Initialize ()
    {
      glClearColor (0.0f, 0.0f, 0.0f, 1.0f);               // Black Background
      glClearDepth (1.0f);                       // Depth Buffer Setup
      glDepthFunc (GL_LEQUAL);                     // The Type Of Depth Testing
      glEnable (GL_DEPTH_TEST);                      // Enable Depth Testing
      glShadeModel (GL_SMOOTH);                      // Select Smooth Shading
      glHint (GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);          // Set Perspective Calculations To Most Accurate
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);             // Draw Our Mesh In Wireframe Mode
    }

    void GraphicsApiAdaptee::Shutdown ()
    {
      return;
    }

    void GraphicsApiAdaptee::AddRenderable ( Simplex::Graphics::Renderable& renderable )
    {
      RenderableMapping renderableMapping = RenderableMapping();
      renderableMapping.Renderable = &renderable;
      mRenderables.push_back( renderableMapping );
      renderable.SetGraphicsApiIndex( mRenderables.size() - 1);
    }

    Simplex::Graphics::Mesh& GraphicsApiAdaptee::GetLoadedMesh ( int index )
    {
      RenderableMapping renderableMapping = mRenderables[index];
      return *renderableMapping.Mesh;
    }

    void GraphicsApiAdaptee::LoadMesh ( int index, Simplex::Graphics::Mesh* mesh )
    {
      RenderableMapping renderableMapping = mRenderables[index];
      renderableMapping.Mesh = mesh;
      renderableMapping.VertexCount = mesh->GetVertexCount();

#ifdef __APPLE__
      glGenVertexArraysAPPLE(1,&renderableMapping.VertexArrayObject);
#else
      glGenVertexArrays(1,&renderableMapping.VertexArrayObject);
#endif

#ifdef __APPLE__
      glBindVertexArrayAPPLE(renderableMapping.VertexArrayObject);
#else
      glBindVertexArray(renderableMapping.VertexArrayObject);
#endif

      glGenBuffers( 1, &renderableMapping.Buffer );
      glBindBuffer( GL_ARRAY_BUFFER, renderableMapping.Buffer);

      // Move to OpenGL Friendly
      GLfloat OpenGLFriendlyVertex[3][3] = {
        OpenGLFriendlyVertex[0][0] = mesh->GetVertexAt(0).x,
        OpenGLFriendlyVertex[0][1] = mesh->GetVertexAt(0).y,
        OpenGLFriendlyVertex[0][2] = mesh->GetVertexAt(0).z,
        OpenGLFriendlyVertex[1][0] = mesh->GetVertexAt(1).x,
        OpenGLFriendlyVertex[1][1] = mesh->GetVertexAt(1).y,
        OpenGLFriendlyVertex[1][2] = mesh->GetVertexAt(1).z,
        OpenGLFriendlyVertex[2][0] = mesh->GetVertexAt(2).x,
        OpenGLFriendlyVertex[2][1] = mesh->GetVertexAt(2).y,
        OpenGLFriendlyVertex[2][2] = mesh->GetVertexAt(2).z
      };
      
      glBufferData( GL_ARRAY_BUFFER, sizeof(OpenGLFriendlyVertex),
        OpenGLFriendlyVertex, GL_STATIC_DRAW);

      glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0,  ((const void*) (0)));
      glEnableVertexAttribArray(0);


      mRenderables[index] = renderableMapping;
    }

    void GraphicsApiAdaptee::Render ( int index )
    {
      RenderableMapping renderableMapping = mRenderables[index];
#ifdef __APPLE__
      glBindVertexArrayAPPLE( renderableMapping.VertexArrayObject );
#else
      glBindVertexArray( renderableMapping.VertexArrayObject );
#endif
      
      glDrawArrays(GL_TRIANGLES, 0, renderableMapping.VertexCount);
    }

    std::vector<Simplex::Graphics::Shader::Info>& GraphicsApiAdaptee::GetLoadedShaders ( int index )
    {
      return mRenderables[index].Shaders;
      
    }

    void GraphicsApiAdaptee::LoadShaders(int index, std::vector<Simplex::Graphics::Shader::Info>& shaders )
    {
        if ( shaders.size() == 0 ) { return; }

        RenderableMapping renderableMapping = mRenderables[index];

        GLuint program = glCreateProgram();

        for(std::vector<Simplex::Graphics::Shader::Info>::iterator it = shaders.begin(); it != shaders.end(); ++it) {
          Simplex::Graphics::Shader::Info shaderInfo = *it;
          shaderInfo.shader = CreateShader ( shaderInfo );

          if ( shaderInfo.shader )
            glAttachShader( program, shaderInfo.shader );  
          
        }
        
        glLinkProgram( program );

        GLint linked;
        glGetProgramiv( program, GL_LINK_STATUS, &linked );
        if ( !linked ) {
          GLsizei len;
          glGetProgramiv( program, GL_INFO_LOG_LENGTH, &len );

          GLchar* log = new GLchar[len+1];
          glGetProgramInfoLog( program, len, &len, log );
          std::cerr << "Shader linking failed: " << log << std::endl;
          delete [] log;

          for(std::vector<Simplex::Graphics::Shader::Info>::iterator it = shaders.begin(); it != shaders.end(); ++it) 
          {
            glDeleteShader( it->shader );
            it->shader = 0;
          }
          // TODO: Throw Exception
          return;
        }
        
        renderableMapping.ShaderProgram = program;
        renderableMapping.Shaders = shaders;
        mRenderables[index] = renderableMapping;
    }

    GLuint GraphicsApiAdaptee::CreateShader ( Simplex::Graphics::Shader::Info& shaderInfo )
    {
      GLuint shader = glCreateShader( ShaderTypeMapping ( shaderInfo.type ) );

      const GLchar* source = ReadShader( shaderInfo.filename );
      if ( source == NULL ) 
      {
          glDeleteShader( shader );
          // TODO: Throw Exception
          return 0;
      }

      glShaderSource( shader, 1, &source, NULL );
      delete [] source;

      glCompileShader( shader );

      GLint compiled;
      glGetShaderiv( shader, GL_COMPILE_STATUS, &compiled );
      if ( !compiled ) {
        GLsizei len;
        glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &len );

        // Use Logger
        GLchar* log = new GLchar[len+1];
        glGetShaderInfoLog( shader, len, &len, log );
        std::cerr << "Shader compilation failed: " << log << std::endl;
        delete [] log;
        
        glDeleteShader( shader );
        // TODO: Throw Exception
        return 0;
      }

      return shader;

    }

    const char* GraphicsApiAdaptee::ReadShader( const char* filename )
    {
    #ifdef WIN32
      FILE* infile;
      fopen_s( &infile, filename, "rb" );
    #else
        FILE* infile = fopen( filename, "rb" );
    #endif // WIN32

        if ( !infile ) {
            std::cerr << "Unable to open file '" << filename << "'" << std::endl;
            return NULL;
        }

        fseek( infile, 0, SEEK_END );
        int len = ftell( infile );
        fseek( infile, 0, SEEK_SET );

        GLchar* source = new GLchar[len+1];

        fread( source, 1, len, infile );
        fclose( infile );

        source[len] = 0;
        
        return const_cast<const GLchar*>(source);
    }

    void GraphicsApiAdaptee::UseShaders ( int index )
    {
      glUseProgram ( mRenderables[index].ShaderProgram );
    }

    GLenum GraphicsApiAdaptee::ShaderTypeMapping ( Simplex::Graphics::Shader::Type type )
    {
      switch ( type )
      {
        case Simplex::Graphics::Shader::VERTEX:
          return GL_VERTEX_SHADER;
        // case Simplex::Graphics::Shader::Type::TESSELLATION_CONTROL:
        //   return GL_TESS_CONTROL_SHADER;
        // case Simplex::Graphics::Shader::Type::TESSELLATION_EVALUATION:
        //   return GL_TESS_EVALUATION_SHADER;
        // case Simplex::Graphics::Shader::Type::GEOMETRY:
        //   return GL_GEOMETRY_SHADER;
        case Simplex::Graphics::Shader::FRAGMENT:
          return GL_FRAGMENT_SHADER;
        // case Simplex::Graphics::Shader::Type::COMPUTE:
        //   return GL_COMPUTE_SHADER;
        case Simplex::Graphics::Shader::NONE:
          return GL_NONE;
        default:
          return GL_NONE;
      }
    }

    void GraphicsApiAdaptee::PrintErrors()
    {
      switch ( glGetError() )
      {
        case GL_NO_ERROR:
          Logger::Log("No OpenGL Error");
          break;
        default:
          Logger::Log("OpenGL Error");
      }
    }
  }
}